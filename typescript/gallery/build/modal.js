var Modal = /** @class */ (function () {
    function Modal() {
        this.root = document.querySelector('.modal');
        this.prevButton = this.root.querySelector('.prev');
        this.nextButton = this.root.querySelector('.next');
        this.image = this.root.querySelector('.modal img');
        this.title = this.root.querySelector('.modal h2');
        this.desc = this.root.querySelector('.modal p');
        this.gallery = null;
        this.isOpen = false;
    }
    return Modal;
}());
export { Modal };
export default new Modal();
// let Modal.root = document.querySelector('.modal');
// let ModalPrevButton = (Modal.root as any).querySelector('.prev');
// let ModalNextButton = (Modal.root as any).querySelector('.next');
// let Modal.image:HTMLImageElement = (Modal.root as any).querySelector('.modal img');
// let ModalTitle = (Modal.root as any).querySelector('.modal h2');
// let ModalDesc = (Modal.root as any).querySelector('.modal p');
// let Modal.gallery: any = null;
// let Modal.isOpen: boolean= false;
//# sourceMappingURL=modal.js.map