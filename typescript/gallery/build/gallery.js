import Modal from './modal.js';
var Gallery = /** @class */ (function () {
    function Gallery(gallery) {
        var _this = this;
        this.gallery = gallery;
        this.currentImage = null;
        Array.prototype.forEach.call(this.gallery.children, function (img) {
            img.addEventListener('click', function () { return _this.open(img); });
            img.addEventListener('keyup', function (e) {
                if (e.key == 'Enter') {
                    _this.open(img);
                }
                else if (Modal.isOpen && e.key == 'Escape') {
                    _this.close();
                }
            });
        });
    }
    Gallery.prototype.setImage = function (src, title, desc) {
        Modal.image.src = src;
        Modal.title.innerHTML = title;
        Modal.desc.innerHTML = desc;
    };
    Gallery.prototype.open = function (opt) {
        Modal.isOpen = true;
        Modal.gallery = opt.closest('.gallery').galleryObj;
        this.setImage(opt.src, opt.title, opt.dataset.description);
        Modal.root.classList.add('open');
        this.currentImage = opt;
        return opt.src;
    };
    ;
    Gallery.prototype.close = function () {
        Modal.isOpen = false;
        Modal.root.classList.remove('open');
        return Modal.image.src;
    };
    ;
    Gallery.prototype.next = function () {
        var nextImage = (this.currentImage.nextElementSibling || this.gallery.firstElementChild);
        this.currentImage = nextImage;
        this.setImage(nextImage.src, nextImage.title, nextImage.dataset.description);
        return nextImage.src;
    };
    ;
    Gallery.prototype.prev = function () {
        var prevImage = (this.currentImage.previousElementSibling || this.gallery.lastElementChild);
        this.currentImage = prevImage;
        this.setImage(prevImage.src, prevImage.title, prevImage.dataset.description);
        return prevImage.src;
    };
    ;
    return Gallery;
}());
export default Gallery;
//# sourceMappingURL=gallery.js.map