import Gallery from './gallery.js';
import Modal from './modal.js';
document.querySelectorAll('.gallery').forEach(function (g) { return g.galleryObj = new Gallery(g); });
Modal.nextButton.addEventListener('click', function (e) { return Modal.gallery.next(); });
Modal.prevButton.addEventListener('click', function (e) { return Modal.gallery.prev(); });
addEventListener('keydown', function (e) {
    if (Modal.isOpen) {
        if (e.key === 'ArrowUp' || e.key === 'ArrowRight')
            Modal.gallery.next();
        else if (e.key === 'ArrowDown' || e.key === 'ArrowLeft')
            Modal.gallery.prev();
    }
});
Modal.root.addEventListener('click', function (e) {
    if (Modal.isOpen && e.target === e.currentTarget)
        Modal.gallery.close();
});
//# sourceMappingURL=main.js.map