var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define("modal", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Modal = /** @class */ (function () {
        function Modal() {
            this.root = document.querySelector('.modal');
            this.prevButton = this.root.querySelector('.prev');
            this.nextButton = this.root.querySelector('.next');
            this.image = this.root.querySelector('.modal img');
            this.title = this.root.querySelector('.modal h2');
            this.desc = this.root.querySelector('.modal p');
            this.gallery = null;
            this.isOpen = false;
        }
        return Modal;
    }());
    exports.Modal = Modal;
    exports.default = new Modal();
});
// let Modal.root = document.querySelector('.modal');
// let ModalPrevButton = (Modal.root as any).querySelector('.prev');
// let ModalNextButton = (Modal.root as any).querySelector('.next');
// let Modal.image:HTMLImageElement = (Modal.root as any).querySelector('.modal img');
// let ModalTitle = (Modal.root as any).querySelector('.modal h2');
// let ModalDesc = (Modal.root as any).querySelector('.modal p');
// let Modal.gallery: any = null;
// let Modal.isOpen: boolean= false;
define("gallery", ["require", "exports", "modal"], function (require, exports, modal_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    modal_js_1 = __importDefault(modal_js_1);
    var Gallery = /** @class */ (function () {
        function Gallery(gallery) {
            var _this = this;
            this.gallery = gallery;
            this.currentImage = null;
            Array.prototype.forEach.call(this.gallery.children, function (img) {
                img.addEventListener('click', function () { return _this.open(img); });
                img.addEventListener('keyup', function (e) {
                    if (e.key == 'Enter') {
                        _this.open(img);
                    }
                    else if (modal_js_1.default.isOpen && e.key == 'Escape') {
                        _this.close();
                    }
                });
            });
        }
        Gallery.prototype.setImage = function (src, title, desc) {
            modal_js_1.default.image.src = src;
            modal_js_1.default.title.innerHTML = title;
            modal_js_1.default.desc.innerHTML = desc;
        };
        Gallery.prototype.open = function (opt) {
            modal_js_1.default.isOpen = true;
            modal_js_1.default.gallery = opt.closest('.gallery').galleryObj;
            this.setImage(opt.src, opt.title, opt.dataset.description);
            modal_js_1.default.root.classList.add('open');
            this.currentImage = opt;
            return opt.src;
        };
        ;
        Gallery.prototype.close = function () {
            modal_js_1.default.isOpen = false;
            modal_js_1.default.root.classList.remove('open');
            return modal_js_1.default.image.src;
        };
        ;
        Gallery.prototype.next = function () {
            var nextImage = (this.currentImage.nextElementSibling || this.gallery.firstElementChild);
            this.currentImage = nextImage;
            this.setImage(nextImage.src, nextImage.title, nextImage.dataset.description);
            return nextImage.src;
        };
        ;
        Gallery.prototype.prev = function () {
            var prevImage = (this.currentImage.previousElementSibling || this.gallery.lastElementChild);
            this.currentImage = prevImage;
            this.setImage(prevImage.src, prevImage.title, prevImage.dataset.description);
            return prevImage.src;
        };
        ;
        return Gallery;
    }());
    exports.default = Gallery;
});
define("main", ["require", "exports", "gallery", "modal"], function (require, exports, gallery_js_1, modal_js_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    gallery_js_1 = __importDefault(gallery_js_1);
    modal_js_2 = __importDefault(modal_js_2);
    document.querySelectorAll('.gallery').forEach(function (g) { return g.galleryObj = new gallery_js_1.default(g); });
    modal_js_2.default.nextButton.addEventListener('click', function (e) { return modal_js_2.default.gallery.next(); });
    modal_js_2.default.prevButton.addEventListener('click', function (e) { return modal_js_2.default.gallery.prev(); });
    addEventListener('keydown', function (e) {
        if (modal_js_2.default.isOpen) {
            if (e.key === 'ArrowUp' || e.key === 'ArrowRight')
                modal_js_2.default.gallery.next();
            else if (e.key === 'ArrowDown' || e.key === 'ArrowLeft')
                modal_js_2.default.gallery.prev();
        }
    });
    modal_js_2.default.root.addEventListener('click', function (e) {
        if (modal_js_2.default.isOpen && e.target === e.currentTarget)
            modal_js_2.default.gallery.close();
    });
});
