/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/gallery.ts":
/*!************************!*\
  !*** ./src/gallery.ts ***!
  \************************/
/*! exports provided: Image, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Image", function() { return Image; });
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal */ "./src/modal.ts");

function getRandomNumber(offset, n) {
    if (offset === void 0) { offset = 500; }
    if (n === void 0) { n = 25; }
    return Math.ceil(Math.random() * n) + offset;
}
var Image = /** @class */ (function () {
    function Image(src, gallery) {
        if (src === void 0) { src = ""; }
        this.gallery = gallery;
        this.dom = document.createElement('img');
        this.dom.src = src;
    }
    Image.prototype.load = function () {
        var _this = this;
        this.dom.addEventListener('click', function () { return _modal__WEBPACK_IMPORTED_MODULE_0__["default"].getModal().open(_this); });
        this.dom.addEventListener('keyup', function (e) {
            if (e.key == 'Enter') {
                _modal__WEBPACK_IMPORTED_MODULE_0__["default"].getModal().open(_this);
            }
            else if (_modal__WEBPACK_IMPORTED_MODULE_0__["default"].isOpen && e.key == 'Escape') {
                _modal__WEBPACK_IMPORTED_MODULE_0__["default"].getModal().close();
            }
        });
        return this.dom;
    };
    Image.prototype.next = function () {
        var _a;
        return (_a = (this.dom.nextElementSibling || this.gallery.dom.firstElementChild)) === null || _a === void 0 ? void 0 : _a.imgObj;
    };
    ;
    Image.prototype.prev = function () {
        var _a;
        return (_a = (this.dom.previousElementSibling || this.gallery.dom.lastElementChild)) === null || _a === void 0 ? void 0 : _a.imgObj;
    };
    ;
    return Image;
}());

var Gallery = /** @class */ (function () {
    function Gallery(dom, n) {
        this.dom = dom;
        this.n = n;
        for (var i = 0; i < n; i++) {
            var seriesNo = getRandomNumber();
            var newImg = new Image("https://picsum.photos/" + seriesNo, this);
            newImg.dom.imgObj = newImg;
            newImg.dom.title = seriesNo;
            newImg.dom.dataset.desc = "This is the description for Image:" + seriesNo;
            newImg.dom.tabIndex = 0;
            this.dom.appendChild(newImg.load());
        }
    }
    return Gallery;
}());
/* harmony default export */ __webpack_exports__["default"] = (Gallery);


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _gallery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gallery */ "./src/gallery.ts");

document.querySelectorAll('.gallery').forEach(function (g) { return g.galleryObj = new _gallery__WEBPACK_IMPORTED_MODULE_0__["default"](g, 5); });


/***/ }),

/***/ "./src/modal.ts":
/*!**********************!*\
  !*** ./src/modal.ts ***!
  \**********************/
/*! exports provided: Modal, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Modal", function() { return Modal; });
var Modal = /** @class */ (function () {
    function Modal() {
        this.isActive = false;
        this.currentImage = null;
        this.rootEl = document.createElement('div');
        this.rootEl.className = 'modal';
        this.prevButtonEl = document.createElement('button');
        this.prevButtonEl.className = 'prev';
        this.nextButtonEl = document.createElement('button');
        this.prevButtonEl.className = 'next';
        this.imageEl = document.createElement('img');
        this.titleEl = document.createElement('h2');
        this.descEl = document.createElement('p');
        this.isOpen = false;
        this.rootEl.innerHTML = "<div class=\"modalInner\">\n    <figure>\n        <figcaption>\n        </figcaption>\n    </figure>\n    <div class=\"buttons\"></div>\n</div>";
    }
    Modal.prototype.getModal = function () {
        var _this = this;
        var _a, _b, _c, _d;
        if (!this.isActive) {
            var figureEl = this.rootEl.getElementsByTagName('figure')[0];
            var figCaptionEl = figureEl.lastElementChild;
            var buttonsEl = figureEl.nextElementSibling;
            figureEl.insertAdjacentElement('afterbegin', this.imageEl);
            (_a = figCaptionEl) === null || _a === void 0 ? void 0 : _a.insertAdjacentElement('afterbegin', this.descEl);
            (_b = figCaptionEl) === null || _b === void 0 ? void 0 : _b.insertAdjacentElement('afterbegin', this.titleEl);
            this.prevButtonEl.innerHTML = '←';
            this.prevButtonEl.setAttribute('aria-label', "Previous Photo");
            this.prevButtonEl.addEventListener('click', function (e) { return _this.prevImage(); });
            (_c = buttonsEl) === null || _c === void 0 ? void 0 : _c.appendChild(this.prevButtonEl);
            this.nextButtonEl.innerHTML = '→';
            this.nextButtonEl.setAttribute('aria-label', "Next Photo");
            this.nextButtonEl.addEventListener('click', function (e) { return _this.nextImage(); });
            (_d = buttonsEl) === null || _d === void 0 ? void 0 : _d.appendChild(this.nextButtonEl);
            document.body.appendChild(this.rootEl);
            addEventListener('keydown', function (e) {
                if (_this.isOpen) {
                    if (e.key === 'ArrowUp' || e.key === 'ArrowRight')
                        _this.nextImage();
                    else if (e.key === 'ArrowDown' || e.key === 'ArrowLeft')
                        _this.prevImage();
                }
            });
            this.rootEl.addEventListener('click', function (e) {
                if (_this.isOpen && e.target === e.currentTarget)
                    _this.close();
            });
            this.isActive = true;
        }
        return this;
    };
    Modal.prototype.setImage = function (imageObj) {
        this.imageEl.src = imageObj.dom.src;
        this.titleEl.innerHTML = imageObj.dom.title;
        this.descEl.innerHTML = imageObj.dom.dataset.desc || 'No Description';
    };
    Modal.prototype.nextImage = function () {
        var _a;
        var _next = (_a = this.currentImage) === null || _a === void 0 ? void 0 : _a.next();
        this.currentImage = _next;
        this.setImage(_next);
    };
    Modal.prototype.prevImage = function () {
        var _a;
        var _prev = (_a = this.currentImage) === null || _a === void 0 ? void 0 : _a.prev();
        this.currentImage = _prev;
        this.setImage(_prev);
    };
    Modal.prototype.open = function (opt) {
        var _img = opt.dom;
        this.currentImage = opt;
        this.isOpen = true;
        this.setImage(opt);
        this.rootEl.classList.add('open');
        return opt.dom.src;
    };
    ;
    Modal.prototype.close = function () {
        this.isOpen = false;
        this.rootEl.classList.remove('open');
        return this.imageEl.src;
    };
    ;
    return Modal;
}());

/* harmony default export */ __webpack_exports__["default"] = (new Modal());


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2dhbGxlcnkudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL21haW4udHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL21vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQTJCO0FBVzNCLFNBQVMsZUFBZSxDQUFDLE1BQVksRUFBRSxDQUFNO0lBQXBCLHFDQUFZO0lBQUUsMEJBQU07SUFDekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7QUFDakQsQ0FBQztBQUVEO0lBR0ksZUFBWSxHQUFnQixFQUFVLE9BQWdCO1FBQTFDLDhCQUFnQjtRQUFVLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFDbEQsSUFBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUN2QixDQUFDO0lBRUQsb0JBQUksR0FBSjtRQUFBLGlCQVVDO1FBVEcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsY0FBTSxxREFBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFVBQUMsQ0FBZ0I7WUFDaEQsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLE9BQU8sRUFBRTtnQkFDbEIsOENBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUM7YUFDL0I7aUJBQU0sSUFBSSw4Q0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLFFBQVEsRUFBRTtnQkFDMUMsOENBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUM1QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxvQkFBSSxHQUFKOztRQUNJLGFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFZLDBDQUFFLE1BQU0sQ0FBQztJQUNuRyxDQUFDO0lBQUEsQ0FBQztJQUVGLG9CQUFJLEdBQUo7O1FBQ0ksYUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQVksMENBQUUsTUFBTSxDQUFDO0lBQ3RHLENBQUM7SUFBQSxDQUFDO0lBQ04sWUFBQztBQUFELENBQUM7O0FBRUQ7SUFFSSxpQkFBbUIsR0FBWSxFQUFVLENBQVM7UUFBL0IsUUFBRyxHQUFILEdBQUcsQ0FBUztRQUFVLE1BQUMsR0FBRCxDQUFDLENBQVE7UUFFOUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QixJQUFJLFFBQVEsR0FBRyxlQUFlLEVBQUUsQ0FBQztZQUNqQyxJQUFJLE1BQU0sR0FBUSxJQUFJLEtBQUssQ0FBQywyQkFBeUIsUUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3ZFLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUMzQixNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUM7WUFDNUIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLHVDQUFxQyxRQUFVO1lBQ3pFLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUN2QztJQUVMLENBQUM7SUFDTCxjQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7QUMzREQ7QUFBQTtBQUErQjtBQUcvQixRQUFRLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUNyQyxXQUFDLElBQUksT0FBQyxDQUFTLENBQUMsVUFBVSxHQUFHLElBQUksZ0RBQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQXpDLENBQXlDLENBQ3JELENBQUM7Ozs7Ozs7Ozs7Ozs7QUNIRjtBQUFBO0FBQUE7SUFZSTtRQUhBLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsaUJBQVksR0FBaUIsSUFBSSxDQUFDO1FBSTlCLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7UUFFaEMsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDO1FBRXJDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLGlKQU16QixDQUFDO0lBQ0osQ0FBQztJQUVELHdCQUFRLEdBQVI7UUFBQSxpQkFzQ0M7O1FBckNHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2hCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0QsSUFBSSxZQUFZLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDO1lBQzdDLElBQUksU0FBUyxHQUFHLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQztZQUU1QyxRQUFRLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMzRCxrQkFBWSwwQ0FBRSxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUMvRCxrQkFBWSwwQ0FBRSxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUVoRSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDbEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO1lBQzlELElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFVBQUMsQ0FBTyxJQUFLLFlBQUksQ0FBQyxTQUFTLEVBQUUsRUFBaEIsQ0FBZ0IsQ0FBQyxDQUFDO1lBQzNFLGVBQVMsMENBQUUsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFFMUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxVQUFDLENBQU8sSUFBSyxZQUFJLENBQUMsU0FBUyxFQUFFLEVBQWhCLENBQWdCLENBQUMsQ0FBQztZQUMzRSxlQUFTLDBDQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBRTFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2QyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsVUFBQyxDQUFDO2dCQUMxQixJQUFJLEtBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFNBQVMsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFlBQVk7d0JBQzdDLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzt5QkFDaEIsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFdBQVcsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFdBQVc7d0JBQ25ELEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztpQkFDeEI7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFVBQUMsQ0FBTztnQkFDMUMsSUFBSSxLQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLGFBQWE7b0JBQzNDLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3hCO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELHdCQUFRLEdBQVIsVUFBUyxRQUFlO1FBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxnQkFBZ0IsQ0FBQztJQUMxRSxDQUFDO0lBRUQseUJBQVMsR0FBVDs7UUFDSSxJQUFJLEtBQUssU0FBRyxJQUFJLENBQUMsWUFBWSwwQ0FBRSxJQUFJLEVBQUU7UUFDckMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVELHlCQUFTLEdBQVQ7O1FBQ0ksSUFBSSxLQUFLLFNBQUcsSUFBSSxDQUFDLFlBQVksMENBQUUsSUFBSSxFQUFFO1FBQ3JDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELG9CQUFJLEdBQUosVUFBSyxHQUFVO1FBQ1gsSUFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQztRQUNuQixJQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQztRQUN4QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO0lBQ3ZCLENBQUM7SUFBQSxDQUFDO0lBRUYscUJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO0lBQzVCLENBQUM7SUFBQSxDQUFDO0lBQ04sWUFBQztBQUFELENBQUM7O0FBRWMsbUVBQUksS0FBSyxFQUFFLEVBQUMiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvbWFpbi50c1wiKTtcbiIsImltcG9ydCBNb2RhbCBmcm9tICcuL21vZGFsJ1xyXG5cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSW1hZ2VJIGV4dGVuZHMgSFRNTEltYWdlRWxlbWVudCB7XHJcbiAgICB0aXRsZTogc3RyaW5nLFxyXG4gICAgaW1nT2JqOiBJbWFnZUksXHJcbiAgICBkYXRhc2V0OiB7XHJcbiAgICAgICAgZGVzY3JpcHRpb246IHN0cmluZ1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRSYW5kb21OdW1iZXIob2Zmc2V0ID0gNTAwLCBuID0gMjUpIHtcclxuICAgIHJldHVybiBNYXRoLmNlaWwoTWF0aC5yYW5kb20oKSAqIG4pICsgb2Zmc2V0O1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgSW1hZ2Uge1xyXG4gICAgZG9tOiBIVE1MSW1hZ2VFbGVtZW50O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHNyYzogc3RyaW5nID0gXCJcIiwgcHJpdmF0ZSBnYWxsZXJ5OiBHYWxsZXJ5KSB7XHJcbiAgICAgICAgdGhpcy5kb20gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKTtcclxuICAgICAgICB0aGlzLmRvbS5zcmMgPSBzcmM7XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZCgpIHtcclxuICAgICAgICB0aGlzLmRvbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IE1vZGFsLmdldE1vZGFsKCkub3Blbih0aGlzKSk7XHJcbiAgICAgICAgdGhpcy5kb20uYWRkRXZlbnRMaXN0ZW5lcigna2V5dXAnLCAoZTogS2V5Ym9hcmRFdmVudCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZS5rZXkgPT0gJ0VudGVyJykge1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuZ2V0TW9kYWwoKS5vcGVuKHRoaXMpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKE1vZGFsLmlzT3BlbiAmJiBlLmtleSA9PSAnRXNjYXBlJykge1xyXG4gICAgICAgICAgICAgICAgTW9kYWwuZ2V0TW9kYWwoKS5jbG9zZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZG9tO1xyXG4gICAgfVxyXG5cclxuICAgIG5leHQoKSB7XHJcbiAgICAgICAgcmV0dXJuICgodGhpcy5kb20ubmV4dEVsZW1lbnRTaWJsaW5nIHx8IHRoaXMuZ2FsbGVyeS5kb20uZmlyc3RFbGVtZW50Q2hpbGQpIGFzIEltYWdlSSk/LmltZ09iajtcclxuICAgIH07XHJcblxyXG4gICAgcHJldigpIHtcclxuICAgICAgICByZXR1cm4gKCh0aGlzLmRvbS5wcmV2aW91c0VsZW1lbnRTaWJsaW5nIHx8IHRoaXMuZ2FsbGVyeS5kb20ubGFzdEVsZW1lbnRDaGlsZCkgYXMgSW1hZ2VJKT8uaW1nT2JqO1xyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FsbGVyeSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGRvbTogRWxlbWVudCwgcHJpdmF0ZSBuOiBudW1iZXIpIHtcclxuICAgICAgICBcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgICAgICBsZXQgc2VyaWVzTm8gPSBnZXRSYW5kb21OdW1iZXIoKTtcclxuICAgICAgICAgICAgbGV0IG5ld0ltZzogYW55ID0gbmV3IEltYWdlKGBodHRwczovL3BpY3N1bS5waG90b3MvJHtzZXJpZXNOb31gLCB0aGlzKTtcclxuICAgICAgICAgICAgbmV3SW1nLmRvbS5pbWdPYmogPSBuZXdJbWc7XHJcbiAgICAgICAgICAgIG5ld0ltZy5kb20udGl0bGUgPSBzZXJpZXNObztcclxuICAgICAgICAgICAgbmV3SW1nLmRvbS5kYXRhc2V0LmRlc2MgPSBgVGhpcyBpcyB0aGUgZGVzY3JpcHRpb24gZm9yIEltYWdlOiR7c2VyaWVzTm99YFxyXG4gICAgICAgICAgICBuZXdJbWcuZG9tLnRhYkluZGV4ID0gMDtcclxuICAgICAgICAgICAgdGhpcy5kb20uYXBwZW5kQ2hpbGQobmV3SW1nLmxvYWQoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufSIsImltcG9ydCBHYWxsZXJ5IGZyb20gJy4vZ2FsbGVyeSdcclxuXHJcblxyXG5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZ2FsbGVyeScpLmZvckVhY2goXHJcbiAgICAgICAgZyA9PiAoZyBhcyBhbnkpLmdhbGxlcnlPYmogPSBuZXcgR2FsbGVyeShnLCA1KVxyXG4pOyIsImltcG9ydCB7SW1hZ2V9IGZyb20gJy4vZ2FsbGVyeSdcclxuXHJcbmV4cG9ydCBjbGFzcyBNb2RhbCB7XHJcblxyXG4gICAgcm9vdEVsOiBIVE1MRGl2RWxlbWVudDtcclxuICAgIHByZXZCdXR0b25FbDogSFRNTEJ1dHRvbkVsZW1lbnQ7XHJcbiAgICBuZXh0QnV0dG9uRWw6IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgaW1hZ2VFbDogSFRNTEltYWdlRWxlbWVudDtcclxuICAgIHRpdGxlRWw6IEhUTUxIZWFkaW5nRWxlbWVudDtcclxuICAgIGRlc2NFbDogSFRNTFBhcmFncmFwaEVsZW1lbnQ7XHJcbiAgICBpc09wZW46IGJvb2xlYW47XHJcbiAgICBpc0FjdGl2ZSA9IGZhbHNlO1xyXG4gICAgY3VycmVudEltYWdlOiBJbWFnZSB8IG51bGwgPSBudWxsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICAgICAgICB0aGlzLnJvb3RFbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgIHRoaXMucm9vdEVsLmNsYXNzTmFtZSA9ICdtb2RhbCc7XHJcblxyXG4gICAgICAgIHRoaXMucHJldkJ1dHRvbkVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYnV0dG9uJyk7XHJcbiAgICAgICAgdGhpcy5wcmV2QnV0dG9uRWwuY2xhc3NOYW1lID0gJ3ByZXYnO1xyXG4gICAgICAgIHRoaXMubmV4dEJ1dHRvbkVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYnV0dG9uJyk7XHJcbiAgICAgICAgdGhpcy5wcmV2QnV0dG9uRWwuY2xhc3NOYW1lID0gJ25leHQnO1xyXG5cclxuICAgICAgICB0aGlzLmltYWdlRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKTtcclxuICAgICAgICB0aGlzLnRpdGxlRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdoMicpO1xyXG4gICAgICAgIHRoaXMuZGVzY0VsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xyXG5cclxuICAgICAgICB0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucm9vdEVsLmlubmVySFRNTCA9IGA8ZGl2IGNsYXNzPVwibW9kYWxJbm5lclwiPlxyXG4gICAgPGZpZ3VyZT5cclxuICAgICAgICA8ZmlnY2FwdGlvbj5cclxuICAgICAgICA8L2ZpZ2NhcHRpb24+XHJcbiAgICA8L2ZpZ3VyZT5cclxuICAgIDxkaXYgY2xhc3M9XCJidXR0b25zXCI+PC9kaXY+XHJcbjwvZGl2PmA7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TW9kYWwoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmlzQWN0aXZlKSB7XHJcbiAgICAgICAgICAgIGxldCBmaWd1cmVFbCA9IHRoaXMucm9vdEVsLmdldEVsZW1lbnRzQnlUYWdOYW1lKCdmaWd1cmUnKVswXTtcclxuICAgICAgICAgICAgbGV0IGZpZ0NhcHRpb25FbCA9IGZpZ3VyZUVsLmxhc3RFbGVtZW50Q2hpbGQ7XHJcbiAgICAgICAgICAgIGxldCBidXR0b25zRWwgPSBmaWd1cmVFbC5uZXh0RWxlbWVudFNpYmxpbmc7XHJcbiAgICBcclxuICAgICAgICAgICAgZmlndXJlRWwuaW5zZXJ0QWRqYWNlbnRFbGVtZW50KCdhZnRlcmJlZ2luJywgdGhpcy5pbWFnZUVsKTtcclxuICAgICAgICAgICAgZmlnQ2FwdGlvbkVsPy5pbnNlcnRBZGphY2VudEVsZW1lbnQoJ2FmdGVyYmVnaW4nLCB0aGlzLmRlc2NFbCk7XHJcbiAgICAgICAgICAgIGZpZ0NhcHRpb25FbD8uaW5zZXJ0QWRqYWNlbnRFbGVtZW50KCdhZnRlcmJlZ2luJywgdGhpcy50aXRsZUVsKTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMucHJldkJ1dHRvbkVsLmlubmVySFRNTCA9ICfihpAnO1xyXG4gICAgICAgICAgICB0aGlzLnByZXZCdXR0b25FbC5zZXRBdHRyaWJ1dGUoJ2FyaWEtbGFiZWwnLCBcIlByZXZpb3VzIFBob3RvXCIpXHJcbiAgICAgICAgICAgIHRoaXMucHJldkJ1dHRvbkVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGU6RXZlbnQpID0+IHRoaXMucHJldkltYWdlKCkpO1xyXG4gICAgICAgICAgICBidXR0b25zRWw/LmFwcGVuZENoaWxkKHRoaXMucHJldkJ1dHRvbkVsKTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMubmV4dEJ1dHRvbkVsLmlubmVySFRNTCA9ICfihpInO1xyXG4gICAgICAgICAgICB0aGlzLm5leHRCdXR0b25FbC5zZXRBdHRyaWJ1dGUoJ2FyaWEtbGFiZWwnLCBcIk5leHQgUGhvdG9cIik7XHJcbiAgICAgICAgICAgIHRoaXMubmV4dEJ1dHRvbkVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGU6RXZlbnQpID0+IHRoaXMubmV4dEltYWdlKCkpO1xyXG4gICAgICAgICAgICBidXR0b25zRWw/LmFwcGVuZENoaWxkKHRoaXMubmV4dEJ1dHRvbkVsKTtcclxuICAgIFxyXG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRoaXMucm9vdEVsKTtcclxuICAgICAgICAgICAgYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pc09wZW4pIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZS5rZXkgPT09ICdBcnJvd1VwJyB8fCBlLmtleSA9PT0gJ0Fycm93UmlnaHQnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHRJbWFnZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKGUua2V5ID09PSAnQXJyb3dEb3duJyB8fCBlLmtleSA9PT0gJ0Fycm93TGVmdCcpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJldkltYWdlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgdGhpcy5yb290RWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZTpFdmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNPcGVuICYmIGUudGFyZ2V0ID09PSBlLmN1cnJlbnRUYXJnZXQpXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5pc0FjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBzZXRJbWFnZShpbWFnZU9iajogSW1hZ2UpIHtcclxuICAgICAgICB0aGlzLmltYWdlRWwuc3JjID0gaW1hZ2VPYmouZG9tLnNyYztcclxuICAgICAgICB0aGlzLnRpdGxlRWwuaW5uZXJIVE1MID0gaW1hZ2VPYmouZG9tLnRpdGxlO1xyXG4gICAgICAgIHRoaXMuZGVzY0VsLmlubmVySFRNTCA9IGltYWdlT2JqLmRvbS5kYXRhc2V0LmRlc2MgfHwgJ05vIERlc2NyaXB0aW9uJztcclxuICAgIH1cclxuXHJcbiAgICBuZXh0SW1hZ2UoKXtcclxuICAgICAgICBsZXQgX25leHQgPSB0aGlzLmN1cnJlbnRJbWFnZT8ubmV4dCgpXHJcbiAgICAgICAgdGhpcy5jdXJyZW50SW1hZ2UgPSBfbmV4dDtcclxuICAgICAgICB0aGlzLnNldEltYWdlKF9uZXh0KVxyXG4gICAgfVxyXG5cclxuICAgIHByZXZJbWFnZSgpe1xyXG4gICAgICAgIGxldCBfcHJldiA9IHRoaXMuY3VycmVudEltYWdlPy5wcmV2KClcclxuICAgICAgICB0aGlzLmN1cnJlbnRJbWFnZSA9IF9wcmV2O1xyXG4gICAgICAgIHRoaXMuc2V0SW1hZ2UoX3ByZXYpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW4ob3B0OiBJbWFnZSk6c3RyaW5nIHtcclxuICAgICAgICBsZXQgX2ltZyA9IG9wdC5kb207XHJcbiAgICAgICAgdGhpcy5jdXJyZW50SW1hZ2UgPSBvcHQ7XHJcbiAgICAgICAgdGhpcy5pc09wZW4gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuc2V0SW1hZ2Uob3B0KTtcclxuICAgICAgICB0aGlzLnJvb3RFbC5jbGFzc0xpc3QuYWRkKCdvcGVuJyk7XHJcbiAgICAgICAgcmV0dXJuIG9wdC5kb20uc3JjO1xyXG4gICAgfTtcclxuXHJcbiAgICBjbG9zZSgpOnN0cmluZyB7XHJcbiAgICAgICAgdGhpcy5pc09wZW4gPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnJvb3RFbC5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW1hZ2VFbC5zcmM7XHJcbiAgICB9O1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBuZXcgTW9kYWwoKTsiXSwic291cmNlUm9vdCI6IiJ9