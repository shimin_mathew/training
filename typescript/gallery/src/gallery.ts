import Modal from './modal'


export interface ImageI extends HTMLImageElement {
    title: string,
    imgObj: ImageI,
    dataset: {
        description: string
    }
}

function getRandomNumber(offset = 500, n = 25) {
    return Math.ceil(Math.random() * n) + offset;
}

export class Image {
    dom: HTMLImageElement;

    constructor(src: string = "", private gallery: Gallery) {
        this.dom = document.createElement('img');
        this.dom.src = src;
    }

    load() {
        this.dom.addEventListener('click', () => Modal.getModal().open(this));
        this.dom.addEventListener('keyup', (e: KeyboardEvent) => {
            if (e.key == 'Enter') {
                Modal.getModal().open(this);
            } else if (Modal.isOpen && e.key == 'Escape') {
                Modal.getModal().close();
            }
        });
        return this.dom;
    }

    next() {
        return ((this.dom.nextElementSibling || this.gallery.dom.firstElementChild) as ImageI)?.imgObj;
    };

    prev() {
        return ((this.dom.previousElementSibling || this.gallery.dom.lastElementChild) as ImageI)?.imgObj;
    };
}

export default class Gallery {

    constructor(public dom: Element, private n: number) {
        
        for (let i = 0; i < n; i++) {
            let seriesNo = getRandomNumber();
            let newImg: any = new Image(`https://picsum.photos/${seriesNo}`, this);
            newImg.dom.imgObj = newImg;
            newImg.dom.title = seriesNo;
            newImg.dom.dataset.desc = `This is the description for Image:${seriesNo}`
            newImg.dom.tabIndex = 0;
            this.dom.appendChild(newImg.load());
        }

    }
}