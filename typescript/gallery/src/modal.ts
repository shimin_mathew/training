import {Image} from './gallery'

export class Modal {

    rootEl: HTMLDivElement;
    prevButtonEl: HTMLButtonElement;
    nextButtonEl: HTMLButtonElement;
    imageEl: HTMLImageElement;
    titleEl: HTMLHeadingElement;
    descEl: HTMLParagraphElement;
    isOpen: boolean;
    isActive = false;
    currentImage: Image | null = null;

    constructor() {

        this.rootEl = document.createElement('div');
        this.rootEl.className = 'modal';

        this.prevButtonEl = document.createElement('button');
        this.prevButtonEl.className = 'prev';
        this.nextButtonEl = document.createElement('button');
        this.prevButtonEl.className = 'next';

        this.imageEl = document.createElement('img');
        this.titleEl = document.createElement('h2');
        this.descEl = document.createElement('p');

        this.isOpen = false;
        this.rootEl.innerHTML = `<div class="modalInner">
    <figure>
        <figcaption>
        </figcaption>
    </figure>
    <div class="buttons"></div>
</div>`;
    }

    getModal() {
        if (!this.isActive) {
            let figureEl = this.rootEl.getElementsByTagName('figure')[0];
            let figCaptionEl = figureEl.lastElementChild;
            let buttonsEl = figureEl.nextElementSibling;
    
            figureEl.insertAdjacentElement('afterbegin', this.imageEl);
            figCaptionEl?.insertAdjacentElement('afterbegin', this.descEl);
            figCaptionEl?.insertAdjacentElement('afterbegin', this.titleEl);
            
            this.prevButtonEl.innerHTML = '←';
            this.prevButtonEl.setAttribute('aria-label', "Previous Photo")
            this.prevButtonEl.addEventListener('click', (e:Event) => this.prevImage());
            buttonsEl?.appendChild(this.prevButtonEl);
            
            this.nextButtonEl.innerHTML = '→';
            this.nextButtonEl.setAttribute('aria-label', "Next Photo");
            this.nextButtonEl.addEventListener('click', (e:Event) => this.nextImage());
            buttonsEl?.appendChild(this.nextButtonEl);
    
            document.body.appendChild(this.rootEl);
            addEventListener('keydown', (e) => {
                if (this.isOpen) {
                    if (e.key === 'ArrowUp' || e.key === 'ArrowRight')
                        this.nextImage();
                    else if (e.key === 'ArrowDown' || e.key === 'ArrowLeft')
                        this.prevImage();
                }
            });
            
            this.rootEl.addEventListener('click', (e:Event) => {
                if (this.isOpen && e.target === e.currentTarget)
                    this.close();
            });
            this.isActive = true;
        }

        return this;
    }

    setImage(imageObj: Image) {
        this.imageEl.src = imageObj.dom.src;
        this.titleEl.innerHTML = imageObj.dom.title;
        this.descEl.innerHTML = imageObj.dom.dataset.desc || 'No Description';
    }

    nextImage(){
        let _next = this.currentImage?.next()
        this.currentImage = _next;
        this.setImage(_next)
    }

    prevImage(){
        let _prev = this.currentImage?.prev()
        this.currentImage = _prev;
        this.setImage(_prev);
    }

    open(opt: Image):string {
        let _img = opt.dom;
        this.currentImage = opt;
        this.isOpen = true;
        this.setImage(opt);
        this.rootEl.classList.add('open');
        return opt.dom.src;
    };

    close():string {
        this.isOpen = false;
        this.rootEl.classList.remove('open');
        return this.imageEl.src;
    };
}

export default new Modal();