"use strict";
// enum COLOR {
//     RED = 1,
//     GREEN = 2,
//     BLUE = 3
// };
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal = /** @class */ (function () {
    function Animal(name, type) {
        if (name === void 0) { name = "no-name"; }
        if (type === void 0) { type = "animal"; }
        this.onparent = ";";
        this.name = name;
        this.type = type;
    }
    Object.defineProperty(Animal.prototype, "onp", {
        get: function () {
            return this.onparent;
        },
        set: function (v) {
            this.onparent = v;
        },
        enumerable: true,
        configurable: true
    });
    return Animal;
}());
var Cat = /** @class */ (function (_super) {
    __extends(Cat, _super);
    function Cat(name) {
        var _this = _super.call(this) || this;
        _this.name = name;
        _this.onparent = 'from-child';
        return _this;
    }
    return Cat;
}(Animal));
var cat = new Animal('puc');
var tcat = new Cat('puc');
//# sourceMappingURL=first.js.map