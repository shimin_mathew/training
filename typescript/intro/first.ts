// enum COLOR {
//     RED = 1,
//     GREEN = 2,
//     BLUE = 3
// };

class Animal {
    name: string;
    private readonly type: string;
    protected onparent: string = ";";
    
    constructor(name: string = "no-name", type: string = "animal") {
        this.name = name;
        this.type= type;
    }

    get onp(): string {
        return this.onparent;
    }

    set onp(v: string) {
        this.onparent = v;
    }
}

class Cat extends Animal {
    constructor(name: string) {
        super();
        this.name = name;
        this.onparent = 'from-child';
    }
}

var cat: Animal = new Animal('puc');
var tcat: Cat = new Cat('puc');